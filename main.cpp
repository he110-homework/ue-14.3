#include <iostream>
#include "string"

int main() {
    std::string homeworkText = "I am serious. And don't call me Shirley!";

    std::cout
        << "Original string: " << homeworkText << std::endl
        << "String length: " << homeworkText.length() << std::endl
        << "First char: " << homeworkText[0] << std::endl
        << "Last char: " << homeworkText[homeworkText.length()-1] << std::endl;

    return 0;
}
